package airline;

/**
 *
 * @author mitpa
 */
public class Airport 
{
    private String Code;
    private String City;

    public Airport(String Code, String City) {
        this.Code = Code;
        this.City = City;
    }

    @Override
    public String toString() {
        return "Airport{" + "Code=" + Code + ", City=" + City + '}';
    }

    public String getCode() {
        return Code;
    }

    public String getCity() {
        return City;
    }
    
    
}
