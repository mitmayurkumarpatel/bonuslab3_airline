
package airline;

/**
 *
 * @author mitpa
 */
public class AirplaneSimulator 
{
    public static void main(String[]args)
    {
        Airline airline = new Airline("mit airline" ,"this is short code");
        airline.toString();
    
        Airplane airplane = new Airplane(1234, 56, "Boeing");
        airplane.toString();
        
        Airport airport = new Airport("This is Code","Toronto");
        airport.toString();
        
        Flight flight = new Flight(12);
        flight.toString();
        
    }
    
}
